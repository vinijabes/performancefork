<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/AuthLog.php');

/**
 * Classe responsável por gerenciar as ações de um administrador
 */
class Admin extends AuthLog {
    public function __construct(){
        parent::__construct();
        $this->load->helper('frontend_helper'); 
        $this->load->model('ImageHandler_model', 'imageModel');
    }

    /**
     * Função que retorna o HTML padrão do controller
     */
    public function index(){
        $this->load->view('html-header-admin');
        $this->load->view('header-admin');
        $this->load->view('home');
        $this->load->view('footer-admin');
    }

    private function crypt ($password){
        $options = ['cost' => 12];
        $password = password_hash($password, PASSWORD_DEFAULT, $options);
        return $password; 
    }

}